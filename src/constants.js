export const STATUS = {
  INPLAN: "in plan",
  INWORK: "in work",
  COMLETE: "complete"
}
export const PRIORITY = {
  NORMAL: "normal",
  HIGH: "high"
}
