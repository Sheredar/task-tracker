import { call, put, takeEvery } from "redux-saga/effects";
import { loadDataSuccess, loadDataError } from "./actions/appActions";

function* fetchData(action) {
  try {
    const data = yield call(() => {
      return fetch(`/${localStorage.getItem("token")}.json`).then(response => {
        return response.json();
      });
    });
    yield put(loadDataSuccess(data));
  } catch (error) {
    yield put(loadDataError());
  }
}

function* watchFetchData() {
  yield takeEvery("LOAD_DATA_REQUEST", fetchData);
}

export default watchFetchData;
