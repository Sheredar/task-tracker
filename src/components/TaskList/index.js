import React, { Component } from "react";
import { Link } from "react-router-dom";
import TaskItem from "../TaskItem/";
import styles from './TaskList.module.css';

export default class TaskList extends Component {
  render() {
    const { visible, dataTask } = this.props;
    return (
      <div className={styles.tasks_wrapper}>
        {dataTask.map(item => (
          <Link to={`/app/${item.id}/`} className={styles.link} key={item.id}>
            <TaskItem data={item} visible={visible} />
          </Link>
        ))}
      </div>
    );
  }
}
