import React, { Component } from "react";
import styles from './TaskItem.module.css';

export default class TaskItem extends Component {
  render() {
    const {
      title,
      plannedTime,
      elapsedTime,
      status,
      priority,
      desc
    } = this.props.data;
    const { visible } = this.props;
    return (
      <>
        <div className={styles.task}>
          <p className={styles.task_title}>{title}</p>
          <p className={styles.task_time}>{`${elapsedTime}/${plannedTime} ч.`}</p>
          <p className={styles.task_priority}>{priority}</p>
          <p className={styles.task_status}>{status}</p>
        </div>
        {visible && (
          <div>
            <p className={styles.task_desc}>{desc}</p>
          </div>
        )}
      </>
    );
  }
}
