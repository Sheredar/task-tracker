import React from "react";
import { DropTarget } from "react-dnd";
import styles from './ScrumTable.module.css';

const boxTarget = {
  drop(props) {
    return { name: props.status };
  }
};

export class KanbanColumn extends React.Component {
  render() {
    return this.props.connectDropTarget(<div className={styles.column_wrapper}>{this.props.children}</div>);
  }
}

KanbanColumn = DropTarget("kanbanItem", boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))(KanbanColumn);
