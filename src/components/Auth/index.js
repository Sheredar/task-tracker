import React, { Component } from "react";
import history from "../../history";
import styles from './Auth.module.css';

export default class Auth extends Component {
  state = {
    username: "",
    password: ""
  };
  handleChangeUserName = e => {
    this.setState({ username: e.target.value });
  };
  handleChangePassword = e => {
    this.setState({ password: e.target.value });
  };
  onFormSubmit = e => {
    e.preventDefault();
    fetch("/pass.json")
      .then(response => {
        return response.json();
      })
      .then(data => {
        const userData = data.find(
          data => data.username === this.state.username
        );
        if (!userData) return;
        if (this.state.password === userData.password) {
          localStorage.setItem("token", this.state.username);
          history.push("/app");
        }
      });
  };
  render() {
    return (
      <div className={styles.autorization_wrapper}>
        <p className={styles.title}>Авторизация</p>
        <form className={styles.form} onSubmit={this.onFormSubmit}>
          <input className={styles.input} placeholder="name" onChange={this.handleChangeUserName} />
          <input className={styles.input} placeholder="pass" onChange={this.handleChangePassword} />
          <button className={styles.button}>Отправить</button>
        </form>
      </div>
    );
  }
}
