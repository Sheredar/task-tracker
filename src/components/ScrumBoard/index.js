import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { KanbanItem } from "../ScrumItem";
import { KanbanColumn } from "../ScrumTable/";
import { setStatus } from "../../actions/scrumActions";
import { loadDataRequest } from "../../actions/appActions";
import { STATUS } from "../../constants.js"
import styles from './ScrumBoard.module.css';

const channels = [
  { id: 1, status: STATUS.INPLAN },
  { id: 2, status: STATUS.INWORK },
  { id: 3, status: STATUS.COMLETE }
];

export class Kanban extends React.Component {
  componentDidMount() {
    if (!this.props.tasks.length) {
      this.props.loadDataAction();
    }
  }
  handleUpdate = (id, status) => {
    const taskStatus = {
      id,
      status
    };
    this.props.setStatusAction(taskStatus);
  };
  render() {
    const { tasks } = this.props;
    return (
      <div className={styles.wrapper}>
        <Link to="/app" className={styles.button}>Назад</Link>
        <section className={styles.section}>
          {channels.map(channel => (
            <KanbanColumn key={channel.id} status={channel.status}>
              <div className={styles.column}>
                <div className={styles.head}>{channel.status}</div>
                <div>
                  {tasks
                    .map(item => (
                      item.status === channel.status &&
                      <KanbanItem
                        key={item.id}
                        id={item.id}
                        onDrop={this.handleUpdate}
                      >
                        <div className={styles.item}>{item.title}</div>
                      </KanbanItem>
                    ))}
                </div>
              </div>
            </KanbanColumn>
          ))}
        </section>
      </div>
    );
  }
}

const mapStateToProps = (store, ownProps) => ({
  tasks: store.dataTask
});

const mapDispatchToProps = {
  setStatusAction: setStatus,
  loadDataAction: loadDataRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Kanban);
