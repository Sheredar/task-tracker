import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { setData } from "../../actions/detailActions";
import { loadDataRequest, addData } from "../../actions/appActions";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import history from "../../history";
import { STATUS, PRIORITY } from "../../constants.js";
import styles from "./DetailTask.module.css";

class DetailTask extends Component {
  state = {
    id: null,
    title: "empty",
    desc: "empty",
    date: new Date(),
    plannedTime: 0,
    elapsedTime: 0,
    priority: PRIORITY.NORMAL,
    status: STATUS.INPLAN
  };
  constructor(props) {
    super(props);
    if (this.props.dataTask) {
      this.state = {
        ...this.props.dataTask,
        date: new Date(this.props.dataTask.date)
      };
    }
  }
  componentDidMount() {
    if (!this.props.data.length) {
      this.props.loadDataAction();
    }
    if (this.props.match.params.id === "create") {
      this.setState({ id: this.props.data.length + 1 }, () => {
        this.props.addDataAction(this.state);
        history.push(`/app/${this.props.data.length + 1}`);
      });
    }
  }
  componentDidUpdate(prevProps) {
    const nextProps = this.props;
    if (nextProps.dataTask && nextProps.dataTask !== prevProps.dataTask) {
      this.setState({
        ...nextProps.dataTask,
        date: new Date(nextProps.dataTask.date)
      });
    }
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleChangePicker = date => {
    this.setState({
      date: date
    });
  };
  handleFormSubmit = e => {
    e.preventDefault();
    this.props.setDataAction(this.state);
    history.push("/app");
  };
  render() {
    if (!this.state) return null;
    const {
      title,
      desc,
      date,
      plannedTime,
      elapsedTime,
      status,
      priority
    } = this.state;
    return (
      <div className={styles.task_wrapper}>
        <form className={styles.form} onSubmit={this.handleFormSubmit}>
          <Link className={styles.link} to="/app">
            Назад
          </Link>
          <div className={styles.inputs_wrapper}>
            <label className={styles.title_wrapper}>
              Название задачи:
              <input
                className={styles.title}
                name="title"
                onChange={this.handleChange}
                value={title}
              />
            </label>
            <label className={styles.desc_wrapper}>
              Описание задачи:
              <input
                className={styles.desc}
                name="desc"
                onChange={this.handleChange}
                value={desc}
              />
            </label>
            <div className={styles.date_n_time_wrapper}>
              <label>
                Дата:
                <div className={styles.date_wrapper}>
                  <DatePicker
                    name="date"
                    className={styles.date}
                    selected={this.state.date}
                    onChange={this.handleChangePicker}
                    dateFormat="d.MM.yy"
                    value={date}
                  />
                </div>
              </label>
              <label>
                Время:
                <div className={styles.time_wrapper}>
                  <input
                    className={styles.elapsedTime}
                    value={elapsedTime}
                    type="number"
                    step="0.1"
                    name="elapsedTime"
                    onChange={this.handleChange}
                  />
                  <span className={styles.span}>/</span>
                  <input
                    className={styles.plannedTime}
                    value={plannedTime}
                    type="number"
                    step="0.1"
                    name="plannedTime"
                    onChange={this.handleChange}
                  />
                  <span className={styles.span}>ч.</span>
                </div>
              </label>
            </div>
            <div className={styles.status_n_priority_wrapper}>
              <label>
                Статус:
                <select
                  className={styles.select}
                  name="status"
                  value={status}
                  onChange={this.handleChange}
                >
                  <option value={STATUS.INPLAN}>in plan</option>
                  <option value={STATUS.INWORK}>in work</option>
                  <option value={STATUS.COMPLETE}>complete</option>
                </select>
              </label>
              <label>
                Приоритет:
                <select
                  className={styles.select}
                  name="priority"
                  value={priority}
                  onChange={this.handleChange}
                >
                  <option value={PRIORITY.NORMAL}>normal</option>
                  <option value={PRIORITY.HIGH}>high</option>
                </select>
              </label>
            </div>
          </div>
          <button className={styles.button} type="submit">
            Сохранить
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (store, ownProps) => ({
  dataTask: store.dataTask.find(
    task => String(task.id) === ownProps.match.params.id
  ),
  data: store.dataTask
});

const mapDispatchToProps = {
  setDataAction: setData,
  loadDataAction: loadDataRequest,
  addDataAction: addData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailTask);
