import React, { Component } from "react";
import { Link } from "react-router-dom";
import styles from './ViewEditor.module.css';

export default class ViewEditor extends Component {
  handleHideDetails = e => {
    this.props.setVisible(false);
  };
  handleShowDetails = e => {
    this.props.setVisible(true);
  };
  render() {
    return (
      <div className={styles.view_wrapper}>
        <div className={styles.view_icon_wrapper}>
          <button className={styles.button} type="button" onClick={this.handleHideDetails}>
            <img className={styles.img} src="list.svg" title="кратко" alt="кратко"></img>
          </button>
          <button className={styles.button} type="button" onClick={this.handleShowDetails}>
            <img className={styles.img} src="files.svg" title="подробно" alt="подробно"></img>
          </button>
          <Link className={styles.scrum_link} to={`/scrum/`}>
            <img className={styles.img} src="table-grid.svg" title="scrum" alt="scrum"></img>
          </Link>
        </div>
        <Link className={styles.add_link} to={`app/create/`}>
          + Add task
        </Link>
      </div>
    );
  }
}
