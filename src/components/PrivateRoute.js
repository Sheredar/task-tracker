import React from "react";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!localStorage.getItem("token")) return <Redirect to="/" />;
  return <Route {...rest} component={Component} />;
};
