import React, { Component } from "react";
import { connect } from "react-redux";
import "../../App.css";
import TaskList from "../TaskList/";
import ViewEditor from "../ViewEditor/";
import Filter from "../Filter/";
import {
  setVisible,
  setFilter,
  setSort,
  loadDataRequest,
  addData
} from "../../actions/appActions";
import styles from './App.module.css';

class App extends Component {
  state = {
    time: null
  };
  sortAdapter = (data) => {
    return data
    .filter(dataTask => dataTask.status.includes(this.props.showStatus))
    .sort((a, b) => {
      switch (this.props.showSort) {
        case "ASC":
          return a.title.localeCompare(b.title);
        case "DESC":
          return b.title.localeCompare(a.title);
        default:
          return 0;
      }
    })
  }
  componentDidMount() {
    if (!this.props.dataTask.length) {
      this.props.loadDataAction();
    }
    this.timer = setInterval(() => {
      this.setState({ time: Date.now() });
    }, 100000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {
    const {
      setVisibleAction,
      setFilterAction,
      setSortAction,
      dataTask,
      visible,
      showSort,
      sortAdapter
    } = this.props;
    return (
      <div className={styles.App}>
        <ViewEditor setVisible={setVisibleAction} />
        <Filter
          setFilter={setFilterAction}
          setSort={setSortAction}
          showSort={showSort}
          sortAdapter={sortAdapter}
        />
      <TaskList dataTask={this.sortAdapter(dataTask)} visible={visible} />
      </div>
    );
  }
}

const mapStateToProps = store => ({
  checkStore: store.checkStore,
  visible: store.visible,
  showStatus: store.showStatus,
  showSort: store.showSort,
  dataTask: store.dataTask
});

const mapDispatchToProps = {
  setVisibleAction: setVisible,
  setFilterAction: setFilter,
  setSortAction: setSort,
  loadDataAction: loadDataRequest,
  addDataAction: addData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
