import React, { Component } from "react";
import styles from './Filter.module.css';
import { STATUS } from "../../constants.js"

export default class Filter extends Component {
  onBtnClickFilter = e => {
    this.props.setFilter(e.target.value);
  };
  onBtnClickSortName = e => {
    this.props.setSort(this.props.showSort === "ASC" ? "DESC" : "ASC");
  };
  onBtnClickSortId = e => {
    this.props.setSort("");
  };
  render() {
    return (
      <div className={styles.filter_wrapper}>
        <button className={styles.button} type="button" onClick={this.onBtnClickSortName}>
          Имя <span className={styles.span}>▼</span>
        </button>
        <div className={styles.time_header}>Время</div>
        <div className={styles.priority_header}>Приоритет</div>
        <select className={styles.select} onChange={this.onBtnClickFilter}>
          <option value="">Статус</option>
          <option value={STATUS.INPLAN}>in plan</option>
          <option value={STATUS.INWORK}>in work</option>
          <option value={STATUS.COMPLETE}>complete</option>
        </select>
      </div>
    );
  }
}
