import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { Provider } from "react-redux";
import "./index.css";
import App from "./components/App/";
import ScrumBoard from "./components/ScrumBoard/";
import history from "./history";
import Auth from "./components/Auth/";
import DetailTask from "./components/DetailTask/";
import * as serviceWorker from "./serviceWorker";
import { Router, Route, Switch } from "react-router-dom";
import { PrivateRoute } from "./components/PrivateRoute";
import rootReducer from "./reducers/rootReducer";
import watchFetchData from "./sagas";

import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watchFetchData);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Auth} />
        <PrivateRoute exact path="/app" component={App} />
        <PrivateRoute exact path="/app/:id/" component={DetailTask} />

        <DndProvider backend={HTML5Backend}>
					<PrivateRoute exact path="/scrum/" component={ScrumBoard} />
				</DndProvider>

      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
