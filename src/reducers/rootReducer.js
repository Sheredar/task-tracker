const initialState = {
  visible: false,
  showStatus: "",
  showSort: "",
  dataTask: [],
  loading: false,
  error: false
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_VISIBLE":
      return { ...state, visible: action.payload };
    case "SET_FILTER":
      return { ...state, showStatus: action.payload };
    case "SET_SORT":
      return { ...state, showSort: action.payload };
    case "LOAD_DATA_REQUEST":
      return { ...state, loading: true, error: false };
    case "LOAD_DATA_SUCCESS":
      return { ...state, dataTask: action.payload };
    case "LOAD_DATA_ERROR":
      return { ...state, loading: false, error: true };
    case "ADD_DATA":
      return {
        ...state,
        dataTask: [...state.dataTask, action.payload]
      };
    case "SET_DATA":
      const { payload: newItem } = action;
      return {
        ...state,
        dataTask: state.dataTask.map(task => {
          if (task.id === newItem.id) {
            return newItem;
          }
          return task;
        })
      };
    case "SET_STATUS":
      const { payload: newStatus } = action;
      return {
        ...state,
        dataTask: state.dataTask.map(task => {
          if (task.id === newStatus.id) {
            return {
              ...task,
              status: newStatus.status
            };
          }
          return task;
        })
      };
    default:
      return state;
  }
}
