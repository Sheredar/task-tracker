export function setVisible(visible) {
  return {
    type: "SET_VISIBLE",
    payload: visible
  };
}

export function setFilter(showStatus) {
  return {
    type: "SET_FILTER",
    payload: showStatus
  };
}

export function setSort(showSort) {
  return {
    type: "SET_SORT",
    payload: showSort
  };
}

export function loadDataRequest() {
  return {
    type: "LOAD_DATA_REQUEST"
  };
}

export function loadDataSuccess(data) {
  return {
    type: "LOAD_DATA_SUCCESS",
    payload: data
  };
}

export function loadDataError() {
  return {
    type: "LOAD_DATA_ERROR"
  };
}

export function addData(data) {
  return {
    type: "ADD_DATA",
    payload: data
  };
}
