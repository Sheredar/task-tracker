export function setStatus(data) {
  return {
    type: "SET_STATUS",
    payload: data
  };
}
